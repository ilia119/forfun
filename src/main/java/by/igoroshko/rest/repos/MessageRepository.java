package by.igoroshko.rest.repos;

import by.igoroshko.rest.model.Message;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface MessageRepository extends CrudRepository<Message, Long> {
	List<Message> findByText(String text);
	List<Message> findByTag(String tag);

}
